/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.utils;

import com.galgo.utils.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * Classe utilizda para manipular um intervalo de tempo.
 * Permite a iteração em subintervalos (split).
 *
 * @author valdemar.arantes
 */
public class TimeInterval implements Iterator<TimeInterval> {

    private Logger log = LoggerFactory.getLogger(TimeInterval.class);
    // Início e fim do intervalo de tempo em milissegundos
    // considerados a partir de 1970-01-01T00:00:00Z
    private long startTime;
    private long endTime;
    private long splitDuration = -1; // em milisegundos
    // objeto utilizado nas iterações por conta do split do intervalor
    private TimeInterval position;

    public TimeInterval() {
    }

    /**
     * Ambos os tempos estão em milisegundos contados a partir de 1970-01-01T00:00:00Z
     *
     * @param startTime Início do intervalo em milisegundos
     * @param endTime   Fim do intervalo em milisegundos
     * @throws ApplicationException Exceção lançada se fim do intervalo for menor ou igual ao início
     */
    public TimeInterval(long startTime, long endTime) {
        if (endTime <= startTime) {
            throw new ApplicationException("O início do inervalo deve ser menor que o fim");
        }
        this.startTime = startTime;
        this.endTime = endTime;
        //log.trace("now={}", now);
    }

    public long getEndTime() {
        return endTime;
    }

    public float getSplitDuration() {
        return splitDuration;
    }

    public void setSplitDuration(final long splitDuration) {
        this.splitDuration = splitDuration;
        if (splitDuration <= 0) {
            log.info("Duração do split é zero. Este split será ignorado.");
        } else if (splitDuration > (endTime - startTime)) {
            log.trace("splitDuration={} > timeInterval={}", splitDuration, endTime - startTime);
            this.splitDuration = -1;
        }
    }

    public long getStartTime() {
        return startTime;
    }

    @Override
    public boolean hasNext() {
        if (position == null) {
            return isSplitted();
        } else if (position.getEndTime() == endTime) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isSplitted() {
        return splitDuration > 0;
    }

    @Override
    public TimeInterval next() {
        long nextStart;
        if (position == null) {
            nextStart = startTime;
        } else {
            nextStart = position.endTime + 1;
        }
        long nextEnd = nextStart + splitDuration;
        if (nextEnd > endTime) {
            nextEnd = endTime;
        }
        if (nextEnd == startTime) {
            nextEnd = startTime + 1;
        }
        position = new TimeInterval(nextStart, nextEnd);
        return position;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
