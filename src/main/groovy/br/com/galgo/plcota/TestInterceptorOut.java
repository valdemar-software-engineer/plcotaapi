package br.com.galgo.plcota;

import com.galgo.utils.ApplicationException;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;

public class TestInterceptorOut extends AbstractPhaseInterceptor<Message> {

    private static final Logger log = LoggerFactory.getLogger(TestInterceptorOut.class);

    public TestInterceptorOut() {
        super(Phase.PRE_STREAM);
    }

    @Override
    public void handleFault(Message message) {
        super.handleFault(message);
        try {
            handleAll(message);
        } catch (ApplicationException e) {
            log.warn(e.getMessage());
        }
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        try {
            handleAll(message);
        } catch (ApplicationException e) {
            log.warn(e.getMessage());
        }
    }

    private void handleAll(Message message) {
        final OutputStream os = message.getContent(OutputStream.class);
        if (os == null) {
            log.info("OutputStream de Message nulo. Retornando...");
            return;
        }

        final CacheAndWriteOutputStream newOut = new CacheAndWriteOutputStream(os);
        message.setContent(OutputStream.class, newOut);
        try {
            StringBuilder sb = new StringBuilder();
            newOut.writeCacheTo(sb);
            log.debug("sb={}", sb);
            if (StringUtils.isBlank(sb.toString())) {
                throw new ApplicationException("Conteudo da mensagem vazio");
            }
        } catch (IOException e) {
            log.error(null, e);
        }


        try {
            Transformer serializer;
            serializer = TransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StringWriter swriter = new StringWriter();
            serializer.transform(new StreamSource(newOut.getInputStream()),
                    new StreamResult(swriter));
            String result = swriter.toString();
            log.debug(result);
        } catch (TransformerConfigurationException e) {
            log.error(null, e);
        } catch (IOException e) {
            log.error(null, e);
        } catch (TransformerException e) {
            log.error(e.getMessage());
        }
    }
}
