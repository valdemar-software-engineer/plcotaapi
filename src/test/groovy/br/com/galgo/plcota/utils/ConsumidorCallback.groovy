package br.com.galgo.plcota.utils

/**
 * Created by valdemar.arantes on 29/03/2016.
 */
public interface ConsumidorCallback {
    void execute(Object obj);
}