package br.com.galgo.plcota;

import br.com.stianbid.schemaplcota.MessageRetornoPLCotaComplexType;
import br.com.stianbid.serviceplcota.ConsumirFaultMsg;
import com.galgo.cxfutils.ws.AmbienteEnum;
import com.galgo.utils.cert_digital.TrustAllCerts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.xml.ws.soap.SOAPFaultException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

public class WSConsumirPLCotaTest {

    private static final Logger log = LoggerFactory.getLogger(WSConsumirPLCotaTest.class);

    @BeforeClass
    public static void beforeClass() {
        TrustAllCerts.doIt();
        //Config.newInstance("plcota", null);
    }

    @Test
    public void invokeListaVazia() {
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 6, 16);
        Date init = cal.getTime();

        cal.set(2014, 6, 17);
        Date fin = cal.getTime();

        try {
            final LocalDateTime ldtEnvioInit = LocalDateTime.of(2014, 6, 16, 20, 30);
            Instant instant = ldtEnvioInit.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioInit = Date.from(instant);

            final LocalDateTime ldtEnvioFin = LocalDateTime.of(2014, 6, 16, 20, 35);
            instant = ldtEnvioFin.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioFin = Date.from(instant);

            WSConsumirPLCota wsConsPLCota = new WSConsumirPLCota(null, null, null, dtEnvioInit, dtEnvioFin, 3, 1, 1);
            wsConsPLCota.setReceiveTimeout(500_000).setAmbiente(AmbienteEnum.HOMOLOGACAO).setUser("SG_0306.SG_03",
                "Galgo111");
            MessageRetornoPLCotaComplexType ret = wsConsPLCota.invoke();
            assert (ret == null) : "O retorno da consulta deveria ter sido PC.0137 (lista vazia)";
        } catch (ConsumirFaultMsg e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }

    }

    @Test
    public void invokeUsuarioInexistente() {
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 6, 16);
        Date init = cal.getTime();

        cal.set(2014, 6, 17);
        Date fin = cal.getTime();

        try {
            final LocalDateTime ldtEnvioInit = LocalDateTime.of(2014, 6, 16, 20, 30);
            Instant instant = ldtEnvioInit.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioInit = Date.from(instant);

            final LocalDateTime ldtEnvioFin = LocalDateTime.of(2014, 6, 16, 20, 35);
            instant = ldtEnvioFin.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioFin = Date.from(instant);

            WSConsumirPLCota wsConsPLCota = new WSConsumirPLCota(null, null, null, dtEnvioInit, dtEnvioFin, 3, 1, 1);
            wsConsPLCota.setReceiveTimeout(500_000).setAmbiente(AmbienteEnum.HOMOLOGACAO).setUser("SG_0306.SG_XX",
                "Galgo111");
            MessageRetornoPLCotaComplexType ret = wsConsPLCota.invoke();
            assert (ret == null) : "O retorno da consulta deveria ter sido PC.0237 (Usuário/Senha inválidos)";
        } catch (ConsumirFaultMsg e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }

    }

    @Test
    public void invokeWithFaultException() {
        System.setProperty("plcota.exception.mock", "PC.MOCK");
        Calendar cal = Calendar.getInstance();
        cal.set(2014, 6, 16);
        Date init = cal.getTime();

        cal.set(2014, 6, 17);
        Date fin = cal.getTime();

        try {
            final LocalDateTime ldtEnvioInit = LocalDateTime.of(2014, 6, 16, 20, 30);
            Instant instant = ldtEnvioInit.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioInit = Date.from(instant);

            final LocalDateTime ldtEnvioFin = LocalDateTime.of(2014, 6, 16, 20, 35);
            instant = ldtEnvioFin.atZone(ZoneId.systemDefault()).toInstant();
            Date dtEnvioFin = Date.from(instant);

            WSConsumirPLCota wsConsPLCota = new WSConsumirPLCota(null, null, null, dtEnvioInit, dtEnvioFin, 3, 1, 1);
            wsConsPLCota.setReceiveTimeout(500_000).setAmbiente(AmbienteEnum.HOMOLOGACAO).setUser("SG_0306.SG_03",
                "Galgo111");
            //wsConsPLCota.setDownloadAllRecords(true);
            MessageRetornoPLCotaComplexType ret = wsConsPLCota.invoke();
            Assert.fail("Uma exceção com código PC.MOCK deveria ter sido lançada");
        } catch (SOAPFaultException e) {
            Assert.assertTrue(true, e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail("Era esperada uma exceção SOAPFaultException");
        } finally {
            System.getProperties().remove("plcota.exception.mock");
        }

    }

    @Test
    public void invokeWithNoParameters() {
        try {
            new WSConsumirPLCota().invoke();
        } catch (NullPointerException e) {
            Assert.assertEquals("Login e Password são obrigatórios", e.getMessage());
        } catch (Exception e) {
            log.error(null, e);
            Assert.fail(e.getMessage());
        }
    }
}
