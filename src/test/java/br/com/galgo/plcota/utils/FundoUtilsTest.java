/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.utils;

import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

/**
 *
 * @author valdemar.arantes
 */
public class FundoUtilsTest {
    @Test public void splitCdSTI() {
        String tst = "1234-5, 2345-6,\n\r3456-7";
        StringBuffer errMsg = new StringBuffer();
        List<Integer> splitCdSTI = FundoUtils.splitCdSTI(tst, errMsg);
        Assert.assertEquals(3, splitCdSTI.size());
        Assert.assertEquals(Integer.valueOf(12345), splitCdSTI.get(0));
    }
}
