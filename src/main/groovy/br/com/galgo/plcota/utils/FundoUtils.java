/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.plcota.utils;

import com.google.common.collect.Lists;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * @author valdemar.arantes
 */
public class FundoUtils {

    /**
     * Quebra uma String em uma lista de códigos STI usando como separadores quaisquer caracteres
     * que não sejam números e hífen.
     *
     * @param cdSTIs
     * @param errMsg
     * @return
     */
    public static final List<Integer> splitCdSTI(String cdSTIs, StringBuffer errMsg) {
        String[] cdSTIStrArr = cdSTIs.split("[^0-9\\-]+");
        final List<Integer> cdSTIList = Lists.newArrayList();
        if (ArrayUtils.isNotEmpty(cdSTIStrArr)) {
            for (int i = 0; i < cdSTIStrArr.length; i++) {
                String cd = StringUtils.trimToNull(cdSTIStrArr[i]);
                if (cd == null) {
                    continue;
                }
                cd = StringUtils.replace(cd, "-", "");
                if (!StringUtils.isNumeric(cd)) {
                    errMsg.append("\ncdSTI=" + cdSTIStrArr[i] + " não é válido");
                    continue;
                }
                cdSTIList.add(Integer.valueOf(cd));
            }
        }
        return cdSTIList;
    }
}
